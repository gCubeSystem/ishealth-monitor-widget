
# Changelog for workspace

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.3.0-SNAPSHOT] - 2021-04-06

Ported to git

## [v1.2.0] - 2015-04-13

Upgrade to GWT 2.7.0 completed


## [v1.0.0] - 2013-01-13

First release